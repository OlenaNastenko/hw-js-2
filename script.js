// Завдання 1

var fruits = ["apple", "banana"];
var count = fruits.push("orange");
console.log(count);
console.log(fruits);

var fruits = ["apple", "banana", "orange"];
var lastFruit = fruits.pop();
console.log(lastFruit); 
console.log(fruits);

var fruits = ["banana", "orange"];
var count = fruits.unshift("apple");
console.log(count); 

var fruits = ["apple", "banana", "orange"];
var firstFruit = fruits.shift();
console.log(firstFruit); 
console.log(fruits);

var fruits = ["apple", "banana", "orange", "kiwi", "pineapple"];
var citrus = fruits.slice(2, 4);
console.log(citrus);
console.log(fruits);

var fruits = ["apple", "banana", "orange", "kiwi", "pineapple"];
fruits.splice(2, 1);
console.log(fruits);

var fruits = ["apple", "banana", "orange"];
var result = fruits.join(' and ');
console.log(result);

var numbers = [1, 2, 3, 4, 5];
numbers.reverse();
console.log(numbers); 

var fruit = ["banana", "apple", "orange"];
fruits.sort();
console.log(fruits); 

var numbers = [4, 2, 1, 3, 5];
numbers.sort(function(a, b) {
  return a - b;
});
console.log(numbers); 

var numbers = [1, 2, 3, 4, 5];
var sum = numbers.reduce(function(total, num) {
  return total + num;
}, 0);
console.log(sum); 


//Завдання 2

function getMinAndMax(array) {
    
  let min = array[0];
  let max = array[0];
  
  for (let i = 1; i < array.length; i++) {
    if (array[i] < min) {
      min = array[i];
    }
    if (array[i] > max) {
      max = array[i];
    }
    }
     
  console.log(`Минимальное значение: ${min}`);
  console.log(`Максимальное значение: ${max}`);
}

    var testArray = [5, 7, 1, 3, 9, 4, 8];
console.log(getMinAndMax(testArray));
    

//Завдання3
    var numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    var first = +prompt("Перше число от 0 до 10");
    var last = +prompt("Останнє число от 0 до 10");
    var numres = numbers.slice(first, last);
    console.log("Обрані елементи: " + numres);
  
    
//Завдання4
var arr = [];
for (var i = 1; i <= 10; i++) {
  var num = +prompt(`Введите число ${i}:`);
  arr.push(num);
}
arr.sort((a, b) => a - b);
console.log(arr);

//Завдання5
var arr = [1, 2, 3, 4, 5];
var searchValue = parseInt(prompt('Введіть значення після якого буде вставка, максимум 5'));
var newValue = parseInt(prompt('введіть значення нового елемента'));
var index = arr.indexOf(searchValue);
if (index !== -1) {
arr.splice(index + 1, 0, newValue);
}
console.log(arr);

//Завдання6
var month = ["січень", "лютий", "березень", "квітень", "травень", "червень", "липень", "серпень", "вересень", "жовтень", "листопад", "грудень"];
var preLastMonth = month[month.length - 2];
console.log(preLastMonth);

//Завдання7
var cities = ["Київ", "Львів", "Харків", "Одеса"];
var populations = [2804000, 724713, 1431000, 1014968];
var result = "";
for (var i = 0; i < cities.length; i++) {
  result += cities[i] + " - " + populations[i] + " жителів\n";
}
console.log(result);


